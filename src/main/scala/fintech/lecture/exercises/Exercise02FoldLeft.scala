package fintech.lecture.exercises

object Exercise02FoldLeft extends App {
  type ??? = Nothing

  def reverse[T](xs: List[T]): List[T] = ???
  def filter[T](xs: List[T])(p: ??? => ???): List[T] = ???
  def map[T](xs: List[T])(f: ??? => ???): List[T] = ???
}
